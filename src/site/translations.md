# Translations
[pawa](https://pawa.im) supports "speaking" in the following languages:

| Language                                                                                | Code    | Status           | Contributors         |
|-----------------------------------------------------------------------------------------|:-------:|:----------------:|:--------------------:|
| English                                                                                 | `en`    | Done             | Kinglester           |
| German                                                                                  | `de`    | Done             | Mathis               |
| [Filipino](https://poeditor.com/projects/po_edit?id_language=219&per_page=20&id=294405) | `fil`   | Incomplete (79%) | pranchiii            |
| French                                                                                  | `fr`    | Done             | CedriCDAR77          |
| Hungarian                                                                               | `hu`    | Done             | Bence                |
| Indonesian                                                                              | `id`    | Done             | Aldiwild77           |
| Italian                                                                                 | `ita`   | Done             | Anko                 |
| Polish                                                                                  | `pl`    | Done             | EverythingerTruten   |
| Portuguese (Brazil)                                                                     | `pt_BR` | Done             | Zeus Portella, icebb |
| Spanish                                                                                 | `es`    | Done             | ChangoMC             |
| [Thai](https://poeditor.com/projects/po_edit?id_language=163&per_page=20&id=294405)     | `tha`   | Incomplete (8%)  | Kariburahack         |
| Vietnamese                                                                              | `vi`    | Done             | taa·mee              |

## Contribute
We need your help! [pawa](https://pawa.im) will only be able to speak other languages if contributed by the community.

- Do **NOT** remove `%s`, these are placeholders
- You can submit partial translations, [pawa](https://pawa.im) will fallback to English

### Properties File

Download the [`translations.properties`](https://gitlab.com/pawabot/pawa/-/blob/master/src/main/resources/translations.properties) and translate each of the sentences. You can either, submit a Merge Request directly to the GitLab repository naming your file `translations_<lang>.properties` OR DM the file on Discord.

### POEditor

Go to [pawa's POEditor](https://poeditor.com/join/project?hash=qQs2GuUoYv) project page, and add a language. I will synchronize the translations on the next release.
